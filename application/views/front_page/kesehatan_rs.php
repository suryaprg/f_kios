            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <?php

                        if($list_menu){
                            if($list_menu->msg_main->status){
                                $detail_data = $list_menu->msg_detail;
                                    $id_layanan = $detail_data->id_layanan->id_jenis_rs;
                                    $nama_jenis_rs = $detail_data->id_layanan->nama_layanan;
                                    $item_layanan = $detail_data->item;
                                    $base_url = $detail_data->url_core;


                                foreach ($item_layanan as $r_item_layanan => $v_item_layanan) {
                                    $title = $v_item_layanan->nama_rumah_sakit;
                                    $id = $v_item_layanan->id_rs;
                                    $foto = $v_item_layanan->foto_rs;
                    ?>

                                    <div class="col-md-4">
                                        <div class="card text-center">
                                            <button style="border: none; background: transparent;" onclick="next_page('<?php print_r($id);?>')">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <img src="<?php print_r($base_url.$foto);?>" alt="homepage" style="width: 100px; height: 100px;"/>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        &nbsp;
                                                    </div>
                                                    <div class="col-lg-12" style="height: 40px;">
                                                        <h4 class="card-title"><?php print_r($title);?></h4>
                                                    </div>
                                                </div>                                
                                            </div>
                                            </button>
                                        </div>
                                    </div>
                                    
                    <?php
                                }
                            }
                        }
                        

                    ?>
                </div>
                               
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

<script type="text/javascript">
    function next_page(param){
        var form = document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("action", "<?php print_r(base_url()."beranda/kesehatan/poli")?>");

        var hiddenField = document.createElement("input");
        hiddenField.setAttribute("type", "hidden");
        hiddenField.setAttribute("name", "id_rs");
        hiddenField.setAttribute("value", param);

        var hiddenField1 = document.createElement("input");
        hiddenField1.setAttribute("type", "hidden");
        hiddenField1.setAttribute("name", "id_jenis_rs");
        hiddenField1.setAttribute("value", "<?php print_r($id_layanan);?>");

        form.appendChild(hiddenField);
        form.appendChild(hiddenField1);

        document.body.appendChild(form);
        form.submit();

        // var http = new XMLHttpRequest();
        // var url = "<?php print_r(base_url()."beranda/kesehatan/poli")?>";
        // var params = "id_rs="+param+"&id_jenis_rs="+"<?php print_r($id_layanan);?>";
        // http.open('POST', url, true);

        // //Send the proper header information along with the request
        // http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

        // http.onreadystatechange = function() {//Call a function when the state changes.
        //     if(http.readyState == 4 && http.status == 200) {
        //         alert(http.responseText);
        //     }
        // }
        // http.send(params);
    }
</script>