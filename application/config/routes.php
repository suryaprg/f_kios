<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

#=============================================================================#
#-------------------------------------------page_kesehatan--------------------#
#=============================================================================#
	#--------------------------------halaman_utama--------------------------------#
		$route['beranda/layanan']	= "front_data/homepage/index_home_page";
	#--------------------------------halaman_utama--------------------------------#

	#--------------------------------page_kios_kesehatan--------------------------------#
		$route['beranda/kesehatan/jenis_rumah_sakit']	= "front_data/kesehatanpage/index_kesehatan_jenis";
		$route['beranda/kesehatan/rumah_sakit']			= "front_data/kesehatanpage/index_kesehatan_rs";
		$route['beranda/kesehatan/poli']				= "front_data/kesehatanpage/index_kesehatan_poli";
	#--------------------------------page_kios_kesehatan--------------------------------#

	#--------------------------------daftar_kesehatan-----------------------------------#
		$route['beranda/kesehatan/pushiden']["post"]	= "front_data/kesehatanpage/form_antrean";
		$route['beranda/kesehatan/sendiden']["post"]	= "front_data/kesehatanpage/send_data";
	#--------------------------------daftar_kesehatan-----------------------------------#
#=============================================================================#
#-------------------------------------------page_kesehatan--------------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------page_kependudukan-----------------#
#=============================================================================#

	#--------------------------------page_kios_kependudukan--------------------------------#
		$route['beranda/kependudukan/jenis']		= "front_data/kependudukanpage/index_kependudukan_jenis";
		$route['beranda/kependudukan/kategori']		= "front_data/kependudukanpage/index_kesehatan_rs";
	#--------------------------------page_kios_kependudukan--------------------------------#

	#--------------------------------daftar_kependudukan-----------------------------------#
		$route['beranda/kependudukan/pushiden']["post"]	= "front_data/kependudukanpage/form_antrean";
		$route['beranda/kependudukan/sendiden']["post"]	= "front_data/kependudukanpage/send_data";
	#--------------------------------daftar_kependudukan-----------------------------------#
#=============================================================================#
#-------------------------------------------page_kependudukan-----------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------page_perijinan--------------------#
#=============================================================================#

	#--------------------------------page_kios_perijinan--------------------------------#
		$route['beranda/perijinan/jenis']			= "front_data/Perijinanpage/index_perijinan_jenis";
		$route['beranda/perijinan/kategori']		= "front_data/Perijinanpage/index_perijinan_kategori";
		$route['beranda/perijinan/sub_kategori']	= "front_data/Perijinanpage/index_perijinan_sub_kategori";
	#--------------------------------page_kios_perijinan--------------------------------#

	#--------------------------------daftar_perijinan-----------------------------------#
		$route['beranda/perijinan/pushiden']["post"]	= "front_data/Perijinanpage/form_antrean";
		$route['beranda/perijinan/sendiden']["post"]	= "front_data/Perijinanpage/send_data";
	#--------------------------------daftar_perijinan-----------------------------------#
#=============================================================================#
#-------------------------------------------page_perijinan--------------------#
#=============================================================================#


#=============================================================================#
#-------------------------------------------page_api_kios---------------------#
#=============================================================================#

	#--------------------------------kesehatan------------------------------------#
		$route['get/api/kesehatan/jenis/json']["post"] 	= "front_api/kesehatanapi/get_data_kesehatan_jenis";
		$route['get/api/kesehatan/rs/json']["post"] 	= "front_api/kesehatanapi/get_data_kesehatan_rs";
		$route['get/api/kesehatan/poli/json']["post"] 	= "front_api/kesehatanapi/get_data_kesehatan_poli";
	#--------------------------------kesehatan------------------------------------#
	
#=============================================================================#
#-------------------------------------------page_api_kios---------------------#
#=============================================================================#