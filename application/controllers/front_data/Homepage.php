<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Homepage extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->library("base_url_serv");
		$this->load->library("response_message");
	}

#=================================================================================================#
#-------------------------------------------Homepage_index----------------------------------------#
#=================================================================================================#
	public function index_home_page(){
		$url = $this->base_url_serv->get_base_url()."get/api/halaman_utama/json";

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);
		curl_close($ch);

		$data["list_menu"] 	= json_decode($result);
		$data["core_url"] 	= $this->base_url_serv->get_base_url();

		$this->load->view("front_page/header", $data);
		$this->load->view("front_page/halaman_utama");
		$this->load->view("front_page/footer");
	}
#=================================================================================================#
#-------------------------------------------Homepage_index----------------------------------------#
#=================================================================================================#

}
?>