<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bpkadapi extends CI_Controller {

	
	public function index(){
		// $this->load->view('welcome_message');
		$url_cek_nik = "http://simpeg.malangkota.go.id/serv/get.php";
		$ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url_cek_nik);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);
        curl_close($ch);

        $data_json = json_decode($result)->var_item;

        $data_array = array();
   		$data_table_array = array("golongan"=>"",
   								"jenis_kelamin"=>"",
   								"agama"=>"",
   								"status"=>"",
   								"jumlah_total"=>""
   								);
   		$str_table_fix = "";
   		$data_array_fix = "";

   		$no = 1;
	   	foreach ($data_json as $r_data_json => $v_data_json) {
	   		$data_array["golongan"][$r_data_json]["category"] = $v_data_json->unit_kerja;
	   		$data_array["golongan"][$r_data_json]["jumlah_golongan_I"] = $v_data_json->jumlah_golongan_I;
	   		$data_array["golongan"][$r_data_json]["jumlah_golongan_II"] = $v_data_json->jumlah_golongan_II;
	   		$data_array["golongan"][$r_data_json]["jumlah_golongan_III"] = $v_data_json->jumlah_golongan_III;
	   		$data_array["golongan"][$r_data_json]["jumlah_golongan_IV"] = $v_data_json->jumlah_golongan_IV;

	   		$data_table_array["golongan"] = $data_table_array["golongan"]."<tr>
	   															<td>".$no."</td>
																<td>".$v_data_json->unit_kerja."</td>
																<td align=\"right\">".$v_data_json->jumlah_golongan_I."</td>
																<td align=\"right\">".$v_data_json->jumlah_golongan_II."</td>
																<td align=\"right\">".$v_data_json->jumlah_golongan_III."</td>
																<td align=\"right\">".$v_data_json->jumlah_golongan_IV."</td>
															</tr>";


	   		$data_array["jenis_kelamin"][$r_data_json]["category"] = $v_data_json->unit_kerja;
	   		$data_array["jenis_kelamin"][$r_data_json]["laki"] = $v_data_json->laki;
	   		$data_array["jenis_kelamin"][$r_data_json]["perempuan"] = $v_data_json->perempuan;

	   		$data_table_array["jenis_kelamin"] = $data_table_array["jenis_kelamin"]."<tr>
																<td>".$no."</td>
																<td>".$v_data_json->unit_kerja."</td>
																<td align=\"right\">".$v_data_json->laki."</td>
																<td align=\"right\">".$v_data_json->perempuan."</td>
															</tr>";

	   		$data_array["agama"][$r_data_json]["category"] = $v_data_json->unit_kerja;
	   		$data_array["agama"][$r_data_json]["agama_islam"] = $v_data_json->agama_islam;
	   		$data_array["agama"][$r_data_json]["agama_kristen"] = $v_data_json->agama_kristen;
	   		$data_array["agama"][$r_data_json]["agama_katholik"] = $v_data_json->agama_katholik;
	   		$data_array["agama"][$r_data_json]["agama_hindu"] = $v_data_json->agama_hindu;
	   		$data_array["agama"][$r_data_json]["agama_budha"] = $v_data_json->agama_budha;

	   		$data_table_array["agama"] = $data_table_array["agama"]."<tr>
	   															<td>".$no."</td>
																<td>".$v_data_json->unit_kerja."</td>
																<td align=\"right\">".$v_data_json->agama_islam."</td>
																<td align=\"right\">".$v_data_json->agama_kristen."</td>
																<td align=\"right\">".$v_data_json->agama_katholik."</td>
																<td align=\"right\">".$v_data_json->agama_hindu."</td>
																<td align=\"right\">".$v_data_json->agama_budha."</td>
															</tr>";

	   		$data_array["status"][$r_data_json]["category"] = $v_data_json->unit_kerja;
	   		$data_array["status"][$r_data_json]["status_kawin"] = $v_data_json->status_kawin;
	   		$data_array["status"][$r_data_json]["status_belum_kawin"] = $v_data_json->status_belum_kawin;

	   		$data_table_array["status"] = $data_table_array["status"]."<tr>
	   															<td>".$no."</td>
																<td>".$v_data_json->unit_kerja."</td>
																<td align=\"right\">".$v_data_json->status_kawin."</td>
																<td align=\"right\">".$v_data_json->status_belum_kawin."</td>
															</tr>";

			$data_array["jumlah_total"][$r_data_json]["category"] = $v_data_json->unit_kerja;
			$data_array["jumlah_total"][$r_data_json]["jml"] = $v_data_json->jumlah_total;

			$data_table_array["jumlah_total"] = $data_table_array["jumlah_total"]."<tr>
	   															<td>".$no."</td>
																<td>".$v_data_json->unit_kerja."</td>
																<td align=\"right\">".$v_data_json->jumlah_total."</td>
															</tr>";
	   						
			$no++;
	   	}
	   	$data["data_json"] 			= json_encode($data_array);
	   	$data["data_table_array"] 	= json_encode($data_table_array);

	   	$this->load->view("bpkad", $data);
	}
}


