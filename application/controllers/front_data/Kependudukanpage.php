<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Kependudukanpage extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->library("base_url_serv");
		$this->load->library("response_message");
        $this->load->library("encrypt");
	}

#=================================================================================================#
#-------------------------------------------kependudukan_jenis------------------------------------#
#=================================================================================================#
    private function validate_kependudukan_jenis(){
        $config_val_input = array(
                array(
                    'field'=>'id_layanan',
                    'label'=>'Id Layanan',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }


    public function index_kependudukan_jenis(){
    	$url = $this->base_url_serv->get_base_url()."get/api/kependudukan/jenis/json";
        
        if($this->validate_kependudukan_jenis()){
            $id_layanan = $this->input->post("id_layanan");

            $param = $this->input->post("id_layanan");
            $fields = array(
                'id_layanan' => $id_layanan
            );

            $postvars = http_build_query($fields);
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, count($fields));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $result = curl_exec($ch);
            curl_close($ch);

            $data["list_menu"]  = json_decode($result);
            $data["core_url"]   = $this->base_url_serv->get_base_url();

            // print_r($data["list_menu"]);
        }

        // print_r($_POST);        
		$this->load->view("front_page/header", $data);
		$this->load->view("antrian/kependudukan/kependudukan_jenis");
		$this->load->view("front_page/footer");
    }
#=================================================================================================#
#-------------------------------------------kependudukan_jenis------------------------------------#
#=================================================================================================#

#=================================================================================================#
#-------------------------------------------kependudukan_kategori---------------------------------#
#=================================================================================================#

    private function validate_kesehatan_rs(){
        $config_val_input = array(
                array(
                    'field'=>'id_layanan',
                    'label'=>'Id',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                ),array(
                    'field'=>'id_jenis',
                    'label'=>'Id Jenis',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function index_kesehatan_rs(){
    	$url = $this->base_url_serv->get_base_url()."/get/api/kependudukan/kategori/json";

    	if($this->validate_kesehatan_rs()){
            // print_r($_POST);
    		$id_layanan = $this->input->post("id_layanan");
            $id_jenis = $this->input->post("id_jenis");
			$fields = array(
			   'id_layanan' => $id_layanan,
               'id_jenis' => $id_jenis
			);

			$postvars = http_build_query($fields);
			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, count($fields));
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			$result = curl_exec($ch);
			curl_close($ch);

			$data["list_menu"] 	= json_decode($result);
			$data["core_url"] 	= $this->base_url_serv->get_base_url();

            // print_r($data["list_menu"]);
			$this->load->view("front_page/header", $data);
			$this->load->view("antrian/kependudukan/kependudukan_kategori");
			$this->load->view("front_page/footer");
    	}else{
    		// redirect(base_url()."beranda/layanan");
    	}
    	
    }
#=================================================================================================#
#-------------------------------------------kependudukan_kategori---------------------------------#
#=================================================================================================#

#=================================================================================================#
#-------------------------------------------kependudukan_form_antrean-----------------------------#
#=================================================================================================#

    private function validate_kesehatan_access_antrean(){
        $config_val_input = array(
                array(
                    'field'=>'id_layanan',
                    'label'=>'id',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'id_jenis',
                    'label'=>'id_jenis',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'id_kategori',
                    'label'=>'id_kategori',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }
    
    public function form_antrean(){
        $data["list_menu"]  = array();
        $data["core_url"]   = $this->base_url_serv->get_base_url();

        if($this->validate_kesehatan_access_antrean()){
            $id_layanan = $this->input->post("id_layanan");
            $id_jenis = $this->input->post("id_jenis");
            $id_kategori = $this->input->post("id_kategori");

            $data["list_menu"]["id_layanan"]    = $id_layanan;
            $data["list_menu"]["id_jenis"]      = $id_jenis;
            $data["list_menu"]["id_kategori"]   = $id_kategori;
        }
        
        $this->load->view("front_page/header", $data);
        $this->load->view("antrian/kependudukan/kependudukan_form_antrean");
        $this->load->view("front_page/footer");
    }
#=================================================================================================#
#-------------------------------------------kependudukan_form_antrean-----------------------------#
#=================================================================================================#

#=================================================================================================#
#-------------------------------------------push_iden---------------------------------------------#
#=================================================================================================#

    private function validate_kesehatan_send_data(){
        $config_val_input = array(
                array(
                    'field'=>'nik',
                    'label'=>'Nomor Induk Kependudukan',
                    'rules'=>'required|exact_length[16]|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'exact_length'=>"%s 16 ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s n ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'nama',
                    'label'=>'Nama Anda',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'waktu',
                    'label'=>'Tanggal Pendaftaran',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'id_layanan',
                    'label'=>'id_layanan',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'id_jenis',
                    'label'=>'id_jenis',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'id_kategori',
                    'label'=>'id_kategori',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'ip_lan',
                    'label'=>'ip_lan',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'ip_public',
                    'label'=>'ip_public',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }
    
    public function send_data(){
        // print_r($_POST);
        $url = $this->base_url_serv->get_base_url()."get/api/kependudukan/insert_antrian";

        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array("nik"=>"","nama"=>"","waktu"=>"","id_layanan"=>"","id_jenis"=>"","id_kategori"=>"", "item_result"=>""); 

        if($this->validate_kesehatan_send_data()){
            
            $nik    = $this->input->post("nik");
            $nama   = $this->input->post("nama");
            $waktu  = $this->input->post("waktu");
            
            $id_layanan  = $this->input->post("id_layanan");
            $id_jenis    = $this->input->post("id_jenis");
            $id_kategori = $this->input->post("id_kategori");

            $ip_lan     = $this->input->post("ip_lan");
            $ip_public  = $this->input->post("ip_public");

            $fields = array(
               'nik'        =>$nik,
               'nama'       =>$nama,
               'waktu'      =>$waktu,
               'id_layanan' =>$id_layanan,
               'id_jenis'   =>$id_jenis,
               'id_kategori'=>$id_kategori,

               'ip_lan'      =>$ip_lan,
               'ip_public'   =>$ip_public
               
            );

            #----------cek_nik-------------
            $url_cek_nik = "http://36.66.195.150:8082/ws/api/v2/ktp/nik/".$nik."/key/ebd7b57afe100856b13267cda51ff83004c80e8e/format/json";
            $ch_nik = curl_init();

            curl_setopt($ch_nik, CURLOPT_URL, $url_cek_nik);
            curl_setopt($ch_nik, CURLOPT_RETURNTRANSFER, true);

            $result_nik = curl_exec($ch_nik);
            curl_close($ch_nik);

            if($result_nik){
                $data_nik = json_decode($result_nik);

                $nik_result = $data_nik->KTP->NIK;
                $nama_result = $data_nik->KTP->NAMA_LGKP;

                if(isset($data_nik->KTP)){
                    if($nik == $nik_result && strtolower($nama) == strtolower($nama_result)){
                        // print_r($_POST);
                        $postvars = http_build_query($fields);
                        $ch = curl_init();

                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_POST, count($fields));
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                        $result = curl_exec($ch);
                        curl_close($ch);

                        // print_r($result);

                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
                        $msg_detail["item_result"] = json_decode($result);
                    }
                }    
            }
            // print_r($fields);
        }else{
            $msg_detail["nik"] = strip_tags(form_error("nik"));
            $msg_detail["nama"] = strip_tags(form_error("nama"));
            $msg_detail["waktu"] = strip_tags(form_error("waktu"));
            $msg_detail["id_layanan"] = strip_tags(form_error("id_layanan"));
            $msg_detail["id_jenis"] = strip_tags(form_error("id_jenis"));
            $msg_detail["id_kategori"] = strip_tags(form_error("id_kategori"));
        }
        
        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        print_r(json_encode($msg_array));
    }
#=================================================================================================#
#-------------------------------------------push_iden---------------------------------------------#
#=================================================================================================#

}
?>