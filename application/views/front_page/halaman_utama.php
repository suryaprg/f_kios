            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <?php

                        if($list_menu){
                            if($list_menu->msg_main->status){
                                $detail_data = $list_menu->msg_detail;

                                    $item_layanan = $detail_data->item;
                                    $base_url = $detail_data->url_core;

                                foreach ($item_layanan as $r_item_layanan => $v_item_layanan) {
                                    $kategori   = $v_item_layanan->kategori;
                                        $title_kategori = $kategori->nama_kategori;
                    ?>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card card-info text-left">
                                        <div class="card-header">
                                            <h3 class="m-b-0 text-white"><i class="mdi mdi-subdirectory-arrow-right"></i> &nbsp;<?=$title_kategori?></h3>                                  
                                        </div>        
                                    </div>
                                </div>
                    <?php
                                    if(isset($v_item_layanan->list_menu)){
                                        $list_menu  = $v_item_layanan->list_menu;
                                        
                                        foreach ($list_menu as $r_list_menu => $v_list_menu) {
                                            $id_page = $v_list_menu->id_page;
                                            $nama_page = $v_list_menu->nama_page;
                                            $foto_page = $v_list_menu->foto_page;
                                            $next_page = $v_list_menu->next_page;                            
                    ?>

                                    <div class="col-md-3">
                                        <div class="card text-center">
                                            <button style="border: none; background: transparent;" onclick="next_page('<?php print_r($id_page);?>', '<?php print_r($next_page);?>')">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <img src="<?php print_r($base_url.$foto_page);?>" alt="homepage" style="width: 100px; height: 100px;"/>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        &nbsp;
                                                    </div>
                                                    <div class="col-lg-12" style="height: 40px;">
                                                        <h4 class="card-title"><?php print_r($nama_page);?></h4>
                                                    </div>
                                                </div>                                
                                            </div>
                                            </button>
                                        </div>
                                    </div>
                                    
                    <?php
                                        }
                                    }

                    ?>
                            </div>
                        </div>
                    <?php
                                }
                            }
                        }
                        

                    ?>
                </div>
                               
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

<script type="text/javascript">
    function next_page(param, url){
        var form = document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("action", "<?php print_r(base_url())?>"+url);

        var hiddenField = document.createElement("input");
        hiddenField.setAttribute("type", "hidden");
        hiddenField.setAttribute("name", "id_layanan");
        hiddenField.setAttribute("value", param);

        form.appendChild(hiddenField);

        document.body.appendChild(form);
        form.submit();
    }
</script>