            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <?php

                        if($list_menu){
                            if($list_menu->msg_main->status){
                                $detail_data = $list_menu->msg_detail;
                                    $id_layanan = $detail_data->id_layanan;
                                    $item_layanan = $detail_data->item;
                                    $base_url = $detail_data->url_core;


                                foreach ($item_layanan as $r_item_layanan => $v_item_layanan) {
                                    $title = $v_item_layanan->nama_layanan;
                                    $id = $v_item_layanan->id_layanan;
                                    $foto = $v_item_layanan->foto;
                    ?>

                                    <div class="col-md-3">
                                        <div class="card text-center">
                                            <button style="border: none; background: transparent;" onclick="next_page('<?php print_r($id);?>')">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <img src="<?php print_r($base_url.$foto);?>" alt="homepage" style="width: 100px; height: 100px;"/>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        &nbsp;
                                                    </div>
                                                    <div class="col-lg-12" style="height: 40px;">
                                                        <h4 class="card-title"><?php print_r($title);?></h4>
                                                    </div>
                                                </div>                                
                                            </div>
                                            </button>
                                        </div>
                                    </div>
                                    
                    <?php
                                }
                            }
                        }
                        

                    ?>
                </div>
                               
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

<script type="text/javascript">
    function next_page(param){
        var form = document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("action", "<?php print_r(base_url()."beranda/kesehatan/rumah_sakit")?>");

        var hiddenField = document.createElement("input");
        hiddenField.setAttribute("type", "hidden");
        hiddenField.setAttribute("name", "id_layanan");
        hiddenField.setAttribute("value", param);

        form.appendChild(hiddenField);

        document.body.appendChild(form);
        form.submit();
    }
</script>